$(document).ready(function() {
	replaceSymbols();
	sliders();	
	fastFilterWithLetters();
	tabs();
	scrollInChat();
	getRating();
	setRating();
	autosizeTextarea();
	addPhoto();
	showPassword();
	selects();
	popups();
	showVariants();
	showUserPopup();
	showChatInTablet();
	openChatInMobile();
	changeMasks();
	addScreen();
	changeDeal();

	$('#old-pass').on('input', function() {
		if ($(this).val().length >= 1) {
			$('.profileEdit-buttons .change').attr('disabled', false);
		} else {
			$('.profileEdit-buttons .change').attr('disabled', true);
		}
	});

	$('#politic').on('change', function() {
		$('#register').prop('disabled', !$(this).prop('checked'))
	});

	$('#pay-select').change(function() {
		$('#pay-btn span').text($('#pay-select option:selected').attr('data-balance'))
	});

	$('.productPage-block .popup-input input').on('input change', function(event) {
		if ($(this).val() <= 1) {
			$(this).val(1)
		}
	});
});

function replaceSymbols() {
	var numberInput = $('.number-input');
	numberInput.each(function() {
		$(this).bind('change keyup input click', function() {
			if (this.value.match(/[^0-9]/g)) {
				this.value = this.value.replace(/[^0-9]/g, '');
			}
		});
	});
}

function sliders() {
	var mainSlider = new Swiper('.index-container', {
		slidesPerView: 1,
		pagination: {
			el: '.index-pagination'
		}
	});

	var similarSlder = new Swiper('.similar-container', {
		slidesPerView: 'auto',
		// spaceBetween: 25,
		loop: true,
		loopedSlides: 4
		// breakpoints: {
		// 	320: {
		// 		slidesPerView: 1,
		// 		spaceBetween: 12,
		// 	},
		// 	768: {
		// 		slidesPerView: 3,
		// 		spaceBetween: 25,
		// 	},
		// 	1024: {
		// 		slidesPerView: 4
		// 	},
		// 	1366: {
		// 		slidesPerView: 3
		// 	},
		// 	1601: {
		// 		slidesPerView: 4
		// 	}
		// }
	})
}

function fastFilterWithLetters() {
	var nav = $('.alphabet a');

	nav.each(function() {
		$(this).on('click', function (e) {
			e.preventDefault();
			var thHref = $(this).text();
			var navSection = $('.games-alph');
			nav.removeClass('active');
			$(this).addClass('active');

			navSection.each(function() {
				if ( thHref === $(this).attr('data-letter') ) {
					$('html,body').stop().animate({ scrollTop: $(this).offset().top - 50 }, 1000);
				}
			});
		});
	});

	// $(window).on('scroll', function() {
	// 	var windscroll = $(window).scrollTop();
	// 	if (windscroll > 0 && $(window).width() > 768) {
	// 		$('.games-alph').each(function(i) {
	// 			if ($(this).position().top <= windscroll ) {
	// 				$('.alphabet a.active').removeClass('active');
	// 				$('.alphabet a').eq(i).addClass('active');
	// 			}
	// 		});
	// 	}
	// }).scroll();
}

function tabs() {
	$('ul.product-ul').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.index').find('div.product-tab').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.help-ul').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.help').find('div.help-tab').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('ul.game-variants').on('click', 'li:not(.active)', function() {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.game').find('div.game-links').removeClass('active').eq($(this).index()).addClass('active');
	});
}

function scrollInChat() {
	$('.chat-body').scrollTop($('.chat-inner').height());
	$('.chatPage-body').scrollTop($('.chatPage-inner').height());
}

function getRating() {
	var rate = $('.rate');
	rate.each(function() {
		var num = +$(this).find('input').val();
		$(this).find(`.star:lt(${num})`).addClass('active');
	});
}

function setRating() {
	var star = $('.rating .star'),
	starlength = $('#star-value'),
	starActive = 0;

	star.each(function() {
		$(this).on('click', function() {
			$(this).addClass('active');
			$(this).prevAll().addClass('active');
			$(this).nextAll().removeClass('active');
			starActive = $('.star.active').length;
			starlength.val( starActive );
		});

		$(this).hover(function() {
			$(this).addClass('hovered');
			$(this).prevAll().addClass('hovered');
			$(this).nextAll().removeClass('hovered');
		}, function() {
			star.removeClass('hovered');
		});
	});
}

function autosizeTextarea() {
	autosize($('textarea'));
}

function addPhoto() {
	$('#file').change(function(e) {
		if (e.target.files[0].size > 2097152) {
			console.log(false)
		} else {
			var file = e.target.files[0];
			var reader = new FileReader();
			reader.readAsDataURL(file);
			reader.onload = function(event) {
				$('.profileEdit-img img').attr('src', event.target.result);
			};
		}
	});
}

function showPassword() {
	$('.input-icon').each(function() {
		$(this).on('click', function() {
			var type = $(this).siblings('input').attr('type') == "text" ? "password" : "text";
			var cl = $(this).siblings('input').attr('type') == "text" ? "input-on" : "input-off";
			if (type === "text") {
				$(this).children('.input-hint').text('Скрыть пароль')
			} else {
				$(this).children('.input-hint').text('Показать пароль')
			}
			$(this).siblings('input').attr('type', type);
			$(this).removeClass('input-on');
			$(this).removeClass('input-off');
			$(this).addClass(cl);
		});
	});
}

function selects() {
	$('.nice').each(function(){
		$(this).niceSelect();
		$(this).niceSelect('update');
	});

	$('.second').each(function() {
		$(this).select2({
			minimumResultsForSearch: Infinity,
			templateResult: formatState,
			templateSelection: format
		});
	});

	$('.second2').each(function() {
		$(this).select2({
			minimumResultsForSearch: Infinity,
			templateResult: secondFormatState,
			templateSelection: secondFormat
		});
	});

}
function format(state) {
	var originalOption = state.element;
	if (!state.id) {
		return $(`<span class='statetext'>${state.text}</span> <span class='statebalance'>${$(originalOption).data('balance')}</span>`);
	} else {
		return $(`<img class='icon' src='${$(originalOption).data('icon')}'> <span class='statetext'>${state.text}</span> <span class='statebalance'>${$(originalOption).data('balance')}</span>`)
	}
}

function formatState(state) {
	var originalOption = state.element;
	if (!state.id) {

	} else {
		return $(`<img class='icon' src='${$(originalOption).data('icon')}'> <span class='statetext'>${state.text}</span> <span class='statebalance'>${$(originalOption).data('balance')}</span>`)
	}
}

function secondFormat(state) {
	var originalOption = state.element;
	if (!state.id) {
		return $(`<span class='statetext'>${state.text}</span>`);
	} else {
		return $(`<img class='icon' src='${$(originalOption).data('icon')}'> <span class='statetext'>${state.text}</span>`)
	}
}

function secondFormatState(state) {
	var originalOption = state.element;
	if (!state.id) {

	} else {
		return $(`<img class='icon' src='${$(originalOption).data('icon')}'> <span class='statetext'>${state.text}</span>`)
	}
}

function popups() {
	var popup = $('.popup');
	var overlay = $('.overlay');
	var width = $(window).width();
	var close = $('.popup-close');
	var popupOpen;
	var popupClose;

	popupOpen = function(popupName){
		$('body').css('width', width);
		$('body').addClass('no-scroll');
		overlay.scrollTop(0)
		overlay.addClass('active');
		popup.removeClass('active');
		$('.'+popupName).addClass('active');
	}
	popupClose = function(){
		popup.removeClass('active');
		overlay.removeClass('active');
		$('body').removeClass('no-scroll');
	}
	close.on('click', function(event){
		event.preventDefault();
		popupClose();
	});
	overlay.on('mousedown', function(event){
		if (event.target !== $('.popup') && $('.popup').has(event.target).length === 0) {
			popupClose();
		}
	});
	$('body').on('click','[data-popup-open]',function(e){
		e.preventDefault();
		var popupName = $(this).data('popup-open');
		popupOpen(popupName);
	});

	$('.popup-request .add-file').change(function(event) {
		$('.popup-request .add-text').text(event.target.files[0].name)
	});
}

function showVariants() {
	var inp = $('.gameSearch');
	inp.each(function() {
		$(this).on('input', function() {
			if ($(this).val().length >= 1) {
				$(this).next('.add-hint').show();	
			} else {
				$(this).next('.add-hint').hide();
			}
		});
	});

	$('.search-input').each(function() {
		$(this).on('input', function(event) {
			if ($(this).val().length >= 1) {
				$(this).siblings('.search-results').show();	
			} else {
				$(this).siblings('.search-results').hide();
			}
		});
	});

	$('.input-help').each(function() {
		$(this).on('input', function() {
			if ($(this).val().length >= 1) {
				$(this).parent().siblings('.help-result').show();	
			} else {
				$(this).parent().siblings('.help-result').hide();
			}
		});
	});
}

function showUserPopup() {
	var user = $('.user'),
	visible = false;

	user.on('click', function(event) {
		event.preventDefault();
		if (!visible) {
			$(this).addClass('active');
			$(this).find('.user-popup').show();
			visible = true;
		} else {
			$(this).removeClass('active');
			$(this).find('.user-popup').hide();
			visible = false;
		}
	});
}

function showChatInTablet() {
	$('.tablet-icon').on('click', function() {
		$('body').toggleClass('no-scroll');
		$('.profile-right').toggleClass('active');
	});
}

function openChatInMobile() {
	$('.chatPage-user').on('click', function() {
		$('.chatPage-user').removeClass('active');
		$(this).addClass('active');
		$(this).parents('.chatPage-users').addClass('transformed');
		$('.chatPage-messages').addClass('transformed');
	});

	$('.chatPage-back').on('click', function() {
		$('.chatPage-users').removeClass('transformed');
		$('.chatPage-messages').removeClass('transformed');
	});
}

function changeMasks() {
	$('#pay').change( function() {

		$('#props').fadeIn(250);
		$('.card-mask').val('')

		let value = $(this).val();

		if (value === '2' || value === '3' ) {
			$('.card-mask').mask('9999 9999 9999 9999', {
				autoclear: false,
				placeholder: ''
			});
			$('.card-mask').attr('placeholder', '0000 0000 0000 0000');
		}

		if (value === '4' || value === '5') {
			$('.card-mask').mask('+7 999 999 99 99', {
				autoclear: false,
				placeholder: ''
			});
			$('.card-mask').attr('placeholder', '+7 000 000 00 00');
		}
		if (value === '6') {
			$('.card-mask').mask('R999999999999', {
				autoclear: false,
				placeholder: ''
			});
			$('.card-mask').attr('placeholder', 'R000000000000');
		} else if (value === '7') {
			$('.card-mask').mask('P999999999999', {
				autoclear: false,
				placeholder: ''
			});
			$('.card-mask').attr('placeholder', 'P000000000000');
		} else if (value === '8') {
			$('.card-mask').mask('Z999999999999', {
				autoclear: false,
				placeholder: ''
			});
			$('.card-mask').attr('placeholder', 'Z000000000000');
		} else if (value === '9') {
			$('.card-mask').mask('E999999999999', {
				autoclear: false,
				placeholder: ''
			});
			$('.card-mask').attr('placeholder', 'E000000000000');
		}

	});


	$('.card-input').mask('9999 9999 9999 9999', {
		autoclear: false,
		placeholder: ''
	});

	$('.qiwi-input').mask('+7 999 999 99 99', {
		autoclear: false,
		placeholder: ''
	});

	$('.wmr-input').mask('R999999999999', {
		autoclear: false,
		placeholder: ''
	});

	$('.wmz-input').mask('Z999999999999', {
		autoclear: false,
		placeholder: ''
	});

	$('.yoomoney-input').mask('999999999999999', {
		autoclear: false,
		placeholder: ''
	})
}

let files = [];

function addScreen() {

	$('#addScreen').change(function(event) {
		readFile(event.target.files[0]);
		files.push(event.target.files[0]);
		$('.add-screen').hide();
	});

	$('#addMore').change(function(event) {
		readFile(event.target.files[0]);
		files.push(event.target.files[0]);
	});

	function readFile(file) {
		let reader = new FileReader();
		reader.onload = e => {
			$('.uploadedFiles label.file').before(createTemplate(e.target.result, file.name));
			$('.uploadedFiles').addClass('active');
		}
		reader.readAsDataURL(file);
	}

	function createTemplate(src, name) {
		return `
		<div class="file">
		<div class="row file-remove">&times;</div>
		<div class="file-img">
		<img src="${src}" alt="${name}">
		</div>
		</div>
		`
	}

	$('body').on('click', '.file-remove', function() {
		let idx = $(this).parent().index();
		files.splice(idx, 1);
		$(this).parent('.file').remove();
		if (!files.length) {
			$('.uploadedFiles').removeClass('active');
			$('.add-screen').show();
		}
	});
}

function changeDeal() {
	const deal = $('.deal');
	const unic = $('.unic-input');

	deal.each(function() {
		$(this).change(function() {
			let dt = $(this).attr('data-active');
			$('.deal-block').removeClass('active');
			$(`.deal-block[data-deal="${dt}"]`).addClass('active');
		});
	});

	unic.each(function() {
		$(this).change(function() {
			let dt = $(this).attr('data-unic');
			$('.unic-block').removeClass('active');
			$(`.unic-block[data-unic="${dt}"]`).addClass('active');
		});
	});

	$('.add-product').on('click', function(event) {
		event.preventDefault();
		const elem = $(this).parents('.popup-row').siblings('.add-products');
		elem.append(template(elem));
	});

	function template(elem) {
		let lng = elem.children().length;
		return ` 
		<div class="popup-row">
		<div class="popup-left">Товар ${lng + 1}</div>
		<div class="row popup-right">
		<div class="add-input">
		<textarea name="desc${lng + 1}" placeholder="Описание товара"></textarea>
		<div class="row file-remove">&times;</div>
		</div>
		</div>
		</div>
		`
	}

	$('body').on('click', '.add-input .file-remove', function(event) {
		event.preventDefault();
		$(this).parents('.popup-row').remove();
	});
}