$(document).ready(function() {
	$('.valid-form').each(function(){
		$(this).validate({
			errorClass: 'bad',
			rules: {
				name: {
					required: true
				},
				phone: {
					required: true,
				},
				email: {
					required: true,
					email: true,
				},
				password: {
					required: true,
					minlength: 6
				},
				newpass: {
					required: true,
				},
				renewpass: {
					required: true,
					equalTo: '#newpass'
				},
				code: {
					required: true
				},
			},
			messages: {
				name: {
					required: 'Обязательное поле'
				},
				phone: {
					required: 'Обязательное поле'
				},
				email: {
					required: 'Обязательное поле',
					email: 'Электронная почта введена неправильно',
				},
				password: {
					required: 'Обязательное поле',
					minlength: 'Минимальное кол-во символов 6'
				},
				newpass: {
					required: 'Обязательное поле',
				},
				renewpass: {
					required: 'Обязательное поле',
					equalTo: 'Пароли не совпадают'
				},
				code: {
					required: 'Код введен неправильно'
				},
			}
		})
	});
});